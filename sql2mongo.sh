#!/bin/bash

unset tokens
unset cmd
unset fieldList
unset tableName
unset condLHS
unset condRHS
unset condOps
unset sortList
unset sortDirection
unset version
tokens=()
fieldList=()
condLHS=()
condRHS=()
condOps=()
sortList=()
sortDirection=()
version="1.0.0"

# Desc:  Segment a tring at white spaces, comma, relational
#        operators (=, >, <, >=, <=, <>) and mathematical 
#        operators (+, -, *, /).  Append the segmented words to
#        array 'tokens'
# Input: A string containing no double-quotes
segmentString()
{
	local str
	local pattToken
	local pattEmpty

	str="$1"
	pattToken='^([[:space:]]*)([0-9A-Za-z_.]+|,|\(|\)|>|>=|<|<=|=|<>|\+|-|\*|\/)(.*)'
	pattEmpty='^[[:space:]]*$'     # match an empty string, or a strin containing only white spaces

	while ! [[ "$str" =~ $pattEmpty ]]
	do
		if [[ "$str" =~ $pattToken ]]; then
			tokens+=("${BASH_REMATCH[2]}")
			str="${BASH_REMATCH[3]}"
		else
			echo "Error: unexpected ${BASH_REMATCH[2]}"
			exit 1
		fi
	done
}

# Desc:  Segment a string and save words in the array "tokens". If the string
#        contains double-quoted sub-strings, they will not be segmented and 
#        be treated as a single token.
# Input: A string to be segmented
tokenize()
{
	local pattDbQuote='"'                           # matches a string containing a double-quote
	local pattDbQuotePair='([^"]*)("[^"]*")(.*)'    # matches a string of any length enclosed by a pair of double-quotes
	local cmd
	local preString                                 # part of cmd before the 1st double-quote
	local quotedString                              # part of cmd in a pair of double-quotes (incl. the dobule-quotes)
	local postString                                # part of cmd after the 2nd double-quote

	cmd="$1"
	while [[ "$cmd" =~ $pattDbQuote ]]
	do
		# check whether there is a pair a double quotes
		if ! [[ "$cmd" =~ $pattDbQuotePair ]]; then
			echo "Error: closing double quotation mark not found"
			exit 1
		fi

		# extract the capture parts
		preString="${BASH_REMATCH[1]}"
		quotedString="${BASH_REMATCH[2]}"
		postString="${BASH_REMATCH[3]}"

		segmentString "$preString"      # segment a string into tokens
		tokens+=("$quotedString")    # treat the quoted string as a single token
		cmd="$postString"

	done
	segmentString "$cmd"
}

# Desc:  Print an error message and quit the program with exit code 1.
# Input: 1st argument is the Index of the offending token.
#        If it is -1, then it means EOL.
#        Optionally provide the expected word as the 2nd argument.
printErrorAndExit()
{
	echo -n "SQL syntax error: unexpected "
	if [ "$1" == -1 ]; then
		echo -n "end of input"
	else
		echo -n "<${tokens[$1]}>"
	fi
	if ! [ "$2" = "" ]; then
		echo ", <$2> expected."
	else
		echo "."
	fi
	exit 1
}

###
# A bundle of helper functions to determine the content of input
# arguments. The function names imply what they do.
###
isSelect()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = SELECT ]
}

isFrom()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = FROM ]
}

isWhere()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = WHERE ]
}

isAnd()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = AND ]
}

isOr()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = OR ]
}

isLike()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = LIKE ]
}

isOrder()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = ORDER ]
}

isBy()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = BY ]
}

isIdentifier()
{
	local pattStartAlpha='^[A-Za-z][0-9A-Za-z_.]*'

	[[ "$1" =~ $pattStartAlpha ]] && ! isSelect "$1" && ! isFrom "$1" && ! isWhere "$1"&& ! isAnd "$1"&& ! isOr "$1"&& ! isLike "$1" && ! isOrder "$1" && ! isBy "$1" && ! isAsc "$1" && ! isDesc "$1"
}

isComma()
{
	[ "$1" = "," ]
}

isAsc()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = ASC ]
}

isDesc()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = DESC ]
}

isString()
{
	local pattString='^"[^"]*"$'
	[[ "$1" =~ $pattString ]]
}

isNumeric()
{
	local pattNumeric='^[+-]?([0-9]*[.])?[0-9]+$'
	[[ "$1" =~ $pattNumeric ]]
}

isTrue()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = TRUE ]
}

isFalse()
{
	[ $(echo "$1" | tr '[:lower:]' '[:upper:]') = FALSE ]
}

isLogicalOperator()
{
	local str=$(echo "$1" | tr '[:lower:]' '[:upper:]')
	[ "$str" == "AND" ] || [ "$str" == "OR" ]
}

isRelationalOperator()
{
	[ "$1" == ">" ] || [ "$1" == ">=" ] || [ "$1" == "<" ] ||[ "$1" == "<=" ] || [ "$1" == "=" ] ||[ "$1" == "<>" ] 
}

#############################
# Main program starts here
#############################

# process command line options
cmd=""
while getopts c:vh option;
do
	case $option in
		c) cmd="$OPTARG";;
		v) echo "sql2mongo $version"
		   echo "Copyright (C) "$(date "+%Y") "KH Chung."
		   echo "License GPLv3+: GNU GPL version 3 or later <htps://gnu.org/licenses/gpl.html>."
		   echo "This is free software: you are free t change and redistribute it."
		   echo "There is NO WARRANTY, to the extent permitted by law."
		   echo
		   echo "Written by KH Chung"
		   exit 0;;
		h) echo "Usage: $0 [-vh] [-c statement]"
		   echo "Convert SQL query statement to MongoDB query statement."
		   echo "Opitons:"
		   echo -e "\t-c statement\tspecify the SQL query statement to be converted"
		   echo -e "\t-h\tdisplay this help and exit"
		   echo -e "\t-v\toutput version information and exit"
		   echo 
		   echo "The general foramt of SQL query statement is:"
		   echo "SELECT f1, f2, ... FROM table WHERE f1 < 1 and f2 = true and f3 = \"a string\" ORDER BY f1 DESC, f2"
		   echo
		   echo "Features:"
		   echo "- Informative message for syntax error"
		   echo "- SQL keywords (SELECT, FROM, ...) can either be in lower or upper case letters"
		   echo "- Support embedded period char (.) in field names"
		   echo "- Allow to use asterisk (*) to retrieve all fields"
		   echo "- WHERE clause and ORDER BY clauses are optional"
		   echo "- Support relational operators >, >=, <, <=, =, <> in WHERE clause"
		   echo "- Support logical operator AND in WHERE clause"
		   echo "- Support ASC and DESC keywards in ORDER BY clause"
		   echo 
		   echo "Restrictions:"
		   echo "- Operators LIKE, IN are not supported"
		   echo "- Logical operator OR, NOT are not supported"
		   echo "- Cannot use brackets to group conditional clauses in WHERE clause"
		   echo "- LHS of conditional clauses must be a field names"
		   echo "- RHS of conditional clauses must be constant values, i.e. string, numeric, TRUE, FALSE,"
		   echo "  expressions are not allowed"
		   echo "- Numeric values must be valid decimal floating point numbers"
		   echo
		   echo "Project home: https://gitlab.com/hkpug/sql2mongo"
		   exit 0;;
	esac
done

if [[ "$cmd" == "" ]]; then
	read -p "SQL statement: " cmd
fi
tokenize "$cmd"

idx=0

# Expecting "select"
if [ $idx -ge ${#tokens[@]} ]; then
	printErrorAndExit -1
elif ! isSelect ${tokens[idx]}; then
	printErrorAndExit $idx "select"
fi
let idx++

# Expecting field list
# grab the 1st field
if [ $idx -ge ${#tokens[@]} ]; then
	printErrorAndExit -1 "field identifier"
fi
if ! isIdentifier "${tokens[idx]}" && ! [[ "${tokens[idx]}" =~ ^\*$  ]]; then
	printErrorAndExit $idx "field identifier 2"
fi
fieldList+=("${tokens[idx]}")

# If 1st field is not "*", optionally grab more fields
if ! [[ "${tokens[idx]}" =~ ^\*$ ]]; then
	let idx++

	# expecting "," and a field
	while [[ $idx -lt ${#tokens[@]} ]] && isComma "${tokens[idx]}"
	do
		let idx++

		if [ $idx -ge ${#tokens[@]} ]; then
			printErrorAndExit -1 "field identifier"
		fi
		if ! isIdentifier "${tokens[idx]}"; then
			printErrorAndExit $idx  "field identifier"
		fi
		fieldList+=("${tokens[idx]}")
		let idx++
	done
else
	let idx++
fi

# Expecting "from"
if [ $idx -ge ${#tokens[@]} ]; then
    printErrorAndExit -1
elif ! isFrom ${tokens[idx]}; then
    printErrorAndExit $idx "from"
fi
let idx++

# Expecting a table name
if [ $idx -ge ${#tokens[@]} ]; then
    printErrorAndExit -1 "table identifier"
fi
if ! isIdentifier ${tokens[idx]}; then
    printErrorAndExit $idx "table identifier"
fi
tableName="${tokens[idx]}"
let idx++

# Optionally expecting "where"
if [ $idx -lt ${#tokens[@]} ] && isWhere ${tokens[idx]}; then
	let idx++

	# expecting a conditional clause
	while true
	do
		# expecting LHS be a string, numeric, true, false, or identifier
		if [ $idx -ge ${#tokens[@]} ]; then
			printErrorAndExit -1 "string, numeric, true, false, or identifier"
		fi
		if ! isString "${tokens[idx]}" && ! isNumeric ${tokens[idx]} && ! isTrue ${tokens[idx]} && ! isFalse ${tokens[idx]} && ! isIdentifier ${tokens[idx]}; then
			printErrorAndExit $idx "string, numeric, true, false, or identifier"
		fi
		condLHS+=("${tokens[idx]}")
		let idx++

		# expecting a relational operator
		if [ $idx -ge ${#tokens[@]} ]; then
			printErrorAndExit -1 "relational operator including >, <, <=, >=, =, <>"
		fi
		if ! isRelationalOperator ${tokens[idx]}; then
			printErrorAndExit $idx "relational operator including >, <, <=, >=, =, <>"
		fi
		condOps+=(${tokens[idx]})
		let idx++

		# expecting RHS be a string, numeric, true, false, or identifier
		if [ $idx -ge ${#tokens[@]} ]; then
			printErrorAndExit -1 "string, numeric, true, false, or identifier"
		fi
		if ! isString "${tokens[idx]}" && ! isNumeric ${tokens[idx]} && ! isTrue ${tokens[idx]} && ! isFalse ${tokens[idx]} && ! isIdentifier ${tokens[idx]}; then
			printErrorAndExit $idx "string, numeric, true, false, or identifier"
		fi
		condRHS+=("${tokens[idx]}")
		let idx++

		# if next token is not an logical operator, leave the loop
		if [ $idx -ge ${#tokens[@]} ] || ! isLogicalOperator ${tokens[idx]}; then
			break
		fi
		let idx++
	done
fi # isWhere

# Optionally expecting "order"
if [ $idx -lt ${#tokens[@]} ] && isOrder ${tokens[idx]}; then
	let idx++

	# expecting "by"
	if [ $idx -ge ${#tokens[@]} ]; then
		printErrorAndExit -1 "by"
        fi
	if ! isBy ${tokens[idx]}; then
		printErrorAndExit $idx "by"
	fi
	let idx++

	# expecting a field list, each field optionally annotated by "asc"/"desc"
	while true
	do
		# expecting a field name
		if [ $idx -ge ${#tokens[@]} ]; then
			printErrorAndExit -1 "field identifier"
		fi
		if ! isIdentifier ${tokens[idx]}; then
			printErrorAndExit $idx "field identifier"
		fi
		sortList+=("${tokens[idx]}")
		let idx++

		# optionally expect "asc"/"desc"
		if [ $idx -lt ${#tokens[@]} ] && (isAsc ${tokens[idx]} || isDesc ${tokens[idx]}) ; then
			sortDirection+=($(echo ${tokens[idx]} | tr '[:lower:]' '[:upper:]'))
			let idx++
		else
			# insert a dummy "asc" to align arrays sortList and sortDirection
			sortDirection+=("ASC")
		fi

		# if next token is not ",", leave the loop
		if [ $idx -ge ${#tokens[@]} ] || ! isComma ${tokens[idx]}; then
            break
        fi
        let idx++
	done
fi

# expecting EOL
if [ $idx -lt ${#tokens[@]} ]; then
	printErrorAndExit $idx "end of input"
fi

output="db.$tableName.find( { "
# construct conditions
for idx in "${!condLHS[@]}"; 
do
	output+="\"${condLHS[idx]}\" : "
	case "${condOps[idx]}" in
		">=") output+="{ \$gte : ${condRHS[idx]} }";;
		">" ) output+="{ \$gt : ${condRHS[idx]} }";;
		"=" ) output+="${condRHS[idx]}" ;;
		"<>") output+="{ \$ne : ${condRHS[idx]} }";;
		"<=") output+="{ \$lte : ${condRHS[idx]} }";;
		"<" ) output+="{ \$lt : ${condRHS[idx]} }";;
		* ) echo "relational operator <${condOps[idx]}> not supported yet"
		    exit 1;;
	esac

	# more condition clause ahead?
	if (( idx < ${#condLHS[@]} - 1 )); then
		output+=", "
	fi
done
output+=" }"

# constuct output fields
if ! [[ ${fieldList[0]} =~ ^\*$ ]]; then
	output+=", { "
	for idx in "${!fieldList[@]}";
	do
		output+="\"${fieldList[idx]}\": 1"

		# more fields ahead?
		if (( idx < ${#fieldList[@]} - 1 )); then
			output+=", "
		fi
	done
	output+=" }"
fi

output+=" )"

# construct sorting fields
if (( ${#sortList[@]} > 0 )); then
	output+=".sort( "
	for idx in "${!sortList[@]}";
	do
		output+="\"${sortList[idx]}\": "
		if [[ ${sortDirection[idx]} == ASC ]]; then
			output+=" 1"
		else
			output+=" -1"
		fi

		# more fields ahead?
		if (( idx < ${#sortList[@]} - 1 )); then
			output+=", "
		fi
	done
	output+=" } )"
fi

echo $output
