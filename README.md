## SQL2Mongo

A bash script to convert SQL query statements to [MongoDB](https://www.mongodb.com/) query statement. The Mongo statement can be directly copied-and-pasted to [Mongosh](https://www.mongodb.com/products/shell).

## How to Build

It is a single bash script. No need to build.

## How to Install

Basically you can just execute the main script `sql2mongo.sh` and that's all. If you are not familiar with executing bash scripts, here is the details.

The only single file required is `sql2mongo.sh`. [Download sql2mongo.sh](https://gitlab.com/hkpug/sql2mongo/-/blob/main/sql2mongo.sh).

You may download the whole project in a [zip](https://gitlab.com/hkpug/sql2mongo/-/archive/main/sql2mongo-main.zip) file which includes useful stuff such as this [README](https://gitlab.com/hkpug/sql2mongo/-/blob/main/README.md) file, the [LICENSE](https://gitlab.com/hkpug/sql2mongo/-/blob/main/LICENSE) information, etc. Unzip the downloaded .zip file to a directory, say, `$HOME/sql2mongo`.

You may need to make the script executable by updating its mode with the command below. If you are not sure, just do it.

```
chmod +x $HOME/sql2mongo/sql2mongo.sh
```

## How to Use

Execute the bash script `sql2mongo.sh`. Assume that you put this script in `$HOME/sql2mongo/`, use this command:

```
$HOME/sql2mongo/sql2mongo.sh
```

If you are already in `$HOME/sql2mongo/`, simply use:

```
./sql2mongo.sh
```

You may consider to update environment variable `PATH` to further simplify the typing:

```
export PATH=$PATH:~/sql2mongo
sql2mongo
```

But to make the change to PATH persisting, you need to append the `export ...` line to your $HOME/.bashrc (create this file if it doesn't exist).

You will be prompted to input a SQL query statement. The script will output the corresponding Mongo query statement which can be used directly in [Mongosh](https://www.mongodb.com/products/shell).

The simplest SQL query:

```
$ ./sql2mongo.sh
SQL statement: select * from inventory
db.inventory.find( { } )
```

Specify the columns to be displayed:

```
$ ./sql2mongo.sh
SQL statement: select name, category, price from inventroy
db.inventroy.find( { }, { "name": 1, "category": 1, "price": 1 } )
```

Add record filtering:

```
$ ./sql2mongo.sh
SQL statement: select name, category, price from inventory where price = 10
db.inventory.find( { "price" : 10 }, { "name": 1, "category": 1, "price": 1 } )
```

Multiple filtering criteria joined with AND:

```
$ ./sql2mongo.sh
SQL statement: select name, category, price from inventory where price = 10 and country = "US"
db.inventory.find( { "price" : 10, "country" : "US" }, { "name": 1, "category": 1, "price": 1 } )
```

We support other relational operators as well:

```
$ ./sql2mongo.sh
SQL statement: select name, category, price from inventory where price >= 10 and country <> "US"
db.inventory.find( { "price" : { $gte : 10 }, "country" : { $ne : "US" } }, { "name": 1, "category": 1, "price": 1 } )
```

Sort the output with respect to multiple columns:

```
$ ./sql2mongo.sh
SQL statement: select name, category, price from inventory where price >= 10 and country <> "US" order by price desc, category
db.inventory.find( { "price" : { $gte : 10 }, "country" : { $ne : "US" } }, { "name": 1, "category": 1, "price": 1 } ).sort( "price": -1, "category": 1 } )
```

## Command Line Options

-c statement
: Specify the SQL query statement to be converted. The script will not prompt you for SQL statement.

-h
: Show the help message.

-v
: Show the version.

## Features

- Informative message for syntax error
- SQL keywords (SELECT, FROM, ...) can either be in lower or upper case letters
- Support embedded period char (.) in field names
- Allow to use asterisk (*) to retrieve all fields
- WHERE clause and ORDER BY clauses are optional
- Support relational operators >, >=, <, <=, =, <> in WHERE clause
- Support logical operator AND in WHERE clause
- Support ASC and DESC keywards in ORDER BY clause

## Restrictions

- Operators LIKE, IN are not supported
- Logical operator OR, NOT are not supported
- Cannot use brackets to group conditional clauses in WHERE clause
- LHS of conditional clauses must be a field names
- RHS of conditional clauses must be constant values, i.e. string, numeric, TRUE, FALSE, expressions are not allowed
- Numeric values must be valid decimal floating point numbers

## Motivation of this project

- It is an opportunity for me to learn bash scripting.

  I wrote dead simple bash scripts before, but not any program more than 20 lines. This project is of both useful to myself and complicate enough to force me to learn skills that I never ever used.
- I come from a SQL world and is new to MongoDB.

  I can write complex SQL queries with various kinds of table joining, aggregation, sub-queries without problem. The query syntax of MongoDB is pretty cryptic to me. I have really hard time to type all those curly braces and memorize the complicated syntax even for simple filtering and sorting. I think I would be much more productive if I could query MongoDB with SQL statements. Then it comes this project.
